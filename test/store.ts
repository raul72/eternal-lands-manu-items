import { Store } from 'vuex';
import items from '~/store/items';

jest.mock('../items.json', () => {
  return require('./items.json');
}, { virtual: true });

const store = new Store({
  modules: {
    items: {
      namespaced: true,
      ...items,
    },
  },
});

export default store;

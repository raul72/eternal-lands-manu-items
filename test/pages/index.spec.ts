import { mount, RouterLinkStub } from '@vue/test-utils';
import landingPage from '~/pages/index.vue';

describe('landingPage', () => {
  const wrapper = mount(landingPage, {
    stubs: {
      NuxtLink: RouterLinkStub,
    },
  });

  test('is a Vue instance', () => {
    expect(wrapper.vm).toBeTruthy();
  });

  test('contains skills list', () => {
    expect(wrapper.text()).toMatchSnapshot();
  });
});

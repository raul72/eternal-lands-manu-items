import { shallowMount, RouterLinkStub } from '@vue/test-utils';
import skillPage from '~/pages/skill/_id.vue';
import localVue from '~/test/localVue';
import store from '~/test/store';

describe('skillPage', () => {
  const wrapper = shallowMount(skillPage, {
    localVue,
    store,
    data: () => {
      return {
        skill: 'Crafting',
      };
    },
    stubs: {
      NuxtLink: RouterLinkStub,
    },
  });

  test('is a Vue instance', () => {
    expect(wrapper.vm).toBeTruthy();
  });

  test('contains skill title', () => {
    expect(wrapper.text()).toContain('Crafting');
  });

  test('contains test item', () => {
    expect(wrapper.text()).toContain('Empty Vial');
  });

  test('computed items prop to return expected item(s)', () => {
    // @ts-ignore
    const items = wrapper.vm.$options.computed.items.call({
      $store: store,
      skill: 'Crafting',
    });
    expect(items.length).toEqual(1);
    expect(items).toMatchObject(
      store.getters['items/getItemsBySkill']('Crafting'),
    );
  });

  test('asyncData accept valid skill', async () => {
    expect(typeof wrapper.vm.$options.asyncData).toMatch('function');
    if (typeof wrapper.vm.$options.asyncData !== 'function') {
      return;
    }

    const asyncData = await wrapper.vm.$options.asyncData({
      ...wrapper.vm.$ssrContext,
      store,
      route: {
        params: {
          id: 'Crafting',
        },
      },
      redirect: jest.fn(),
    });

    // @ts-ignore
    expect(asyncData?.skill).toMatch('Crafting');
  });

  test('redirect with incorrect slug', async () => {
    expect(typeof wrapper.vm.$options.asyncData).toMatch('function');
    if (typeof wrapper.vm.$options.asyncData !== 'function') {
      return;
    }

    const redirect = jest.fn();

    await wrapper.vm.$options.asyncData({
      ...wrapper.vm.$ssrContext,
      store,
      route: {
        params: {
          id: 'Foobar',
        },
      },
      redirect,
    });

    expect(redirect).toBeCalled();
  });
});

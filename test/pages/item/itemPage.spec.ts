import { shallowMount } from '@vue/test-utils';
import itemPage from '~/pages/item/_id.vue';
import localVue from '~/test/localVue';
import store from '~/test/store';

describe('itemPage', () => {
  const wrapper = shallowMount(itemPage, {
    localVue,
    store,
    data: () => {
      return {
        item: store.getters['items/getItemBySlug']('potion-of-mana'),
      };
    },
  });

  test('is a Vue instance', () => {
    expect(wrapper.vm).toBeTruthy();
    expect(wrapper.text()).toContain('Potion of Mana');
  });

  test('asyncData should provide correct item for correct slug', async () => {
    expect(typeof wrapper.vm.$options.asyncData).toMatch('function');
    if (typeof wrapper.vm.$options.asyncData !== 'function') {
      return;
    }

    const asyncData = await wrapper.vm.$options.asyncData({
      ...wrapper.vm.$ssrContext,
      store,
      route: {
        params: {
          id: 'potion-of-mana',
        },
      },
      redirect: jest.fn(),
    });

    // @ts-ignore
    expect(asyncData?.item?.name).toMatch('Potion of Mana');
  });

  test('asyncData should redirect with incorrect slug', async () => {
    expect(typeof wrapper.vm.$options.asyncData).toMatch('function');
    if (typeof wrapper.vm.$options.asyncData !== 'function') {
      return;
    }

    const redirect = jest.fn();

    await wrapper.vm.$options.asyncData({
      ...wrapper.vm.$ssrContext,
      store,
      route: {
        params: {
          id: 'foo-bar',
        },
      },
      redirect,
    });

    expect(redirect).toBeCalled();
  });
});

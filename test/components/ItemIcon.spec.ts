import { mount } from '@vue/test-utils';
import ItemIcon from '~/components/ItemIcon.vue';

describe('ItemIcon', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(ItemIcon, {
      propsData: {
        name: 'Test Item',
      },
    });
    expect(wrapper.vm).toBeTruthy();
  });
});

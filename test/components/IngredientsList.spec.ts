import { mount } from '@vue/test-utils';
import IngredientsList from '~/components/IngredientsList.vue';
import localVue from '~/test/localVue';
import store from '~/test/store';


describe('IngredientsList', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(IngredientsList, {
      localVue,
      store,
      propsData: {
        ingredients: {
          foo: 1,
          bar: 2,
        },
      },
    });
    expect(wrapper.vm).toBeTruthy();
  });
});

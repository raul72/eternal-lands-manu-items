import { mount } from '@vue/test-utils';
import IngredientRow from '~/components/IngredientRow.vue';
import localVue from '~/test/localVue';
import store from '~/test/store';

describe('IngredientRow', () => {
  test('is a Vue instance', () => {
    const wrapper = mount(IngredientRow, {
      localVue,
      store,
      propsData: {
        name: 'Foo',
        amount: 2,
      },
    });
    expect(wrapper.vm).toBeTruthy();
  });
});

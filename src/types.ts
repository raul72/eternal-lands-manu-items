export enum Skill {
  Alchemy = 'Alchemy',
  Potion = 'Potion',
  Summoning = 'Summoning',
  Manufacture = 'Manufacture',
  Crafting = 'Crafting',
  Engineering = 'Engineering',
  Tailoring = 'Tailoring',
}

export enum Nexus {
  Animal = 'Animal',
  Artificial = 'Artificial',
  Human = 'Human',
  Inorganic = 'Inorganic',
  Magic = 'Magic',
  Vegetal = 'Vegetal',
}

export interface Item {
  name: string;
  slug: string;
  produced: number;
  food?: number;
  items?: Record<string, number>;
  tools?: string[];
  nexuses?: Partial<Record<Nexus, number>>;
  xp?: Partial<Record<Skill, number>>;
  skills?: Partial<Record<Skill, number>>;
  knowledge?: string[];
}

import { access, constants, readFileSync, writeFileSync } from 'fs';
import axios from 'axios';
import { DATA_FILE_HTML, DATA_URL, ENABLE_CACHE } from './config';

const cacheFileExists = (): Promise<boolean> => new Promise(resolve => {
  access(DATA_FILE_HTML, constants.F_OK, (err) => {
    resolve(!err);
  });
});

export default async (): Promise<string> => {
  if (ENABLE_CACHE && await cacheFileExists()) {
    return readFileSync(DATA_FILE_HTML).toString();
  }

  const response = await axios.get(DATA_URL);
  writeFileSync(DATA_FILE_HTML, response.data);
  return response.data;
};

import { writeFileSync } from 'fs';
import { DATA_FILE_JSON } from '~/src/DataParser/config';
import parseData from '~/src/DataParser/parseData';
import getData from '~/src/DataParser/getData';

const main = async (): Promise<void> => {
  const html = await getData();
  const items = parseData(html);
  writeFileSync(DATA_FILE_JSON, JSON.stringify(items));
};

export default main();

import { Skill, Nexus, Item } from '~/src/types';

enum Sections {
  items = '<b>Items needed:</b>',
  tools = '<b>Items required to have:</b>',
  nexuses = '<b>Required Nexuses:</b>',
  xp = '<b>Experience given:</b>',
  skills = '<b>Recommended Skills:</b>',
  knowledge = '<b>Knowledge needed</b>:',
}

const slugs: string[] = [];
const getSlug = (str: string): string => {
  let slug = str
    .toLowerCase()
    .replace(/ /g, '-')
    .replace(/\//g, '-')
    .replace(/---/g, '-');
  // ensure each slug is unique
  let counter = 0;
  while (slugs.includes(slug)) {
    counter++;
    slug = `${slug}-${counter}`;
  }
  slugs.push(slug);
  return slug;
};

/**
 * first row, match item name and how many items are produced
 * @param html e.g "<b>Potion of Mana (1)</b>"
 */
const parseFirstRow = (html: string): Item => {
  const match = html.match('^<b>([a-zA-Z&/ -]*) \\(([0-9]*)\\)</b>$');
  if (!match) {
    throw new Error(`Failed to match item name from ${html}`);
  }

  return {
    name: match[1],
    slug: getSlug(match[1]),
    produced: Number(match[2]),
  };
};

const parseItem = (itemAsString: string): Item => {
  const rows = itemAsString.split('<br>');
  const item = parseFirstRow(rows.shift() || '');

  let openSection: Sections | undefined;
  rows.forEach((row) => {
    const foodMatch = row.match('^<b>Food subtracted:</b> ([0-9]*)$');
    if (foodMatch) {
      item.food = Number(foodMatch[1]);
      return;
    }

    for (const section of Object.values(Sections)) {
      if (row === section) {
        openSection = section;
        return;
      }
    }

    switch (openSection) {
    case Sections.items: {
      const itemsNeededMatch = row.match('^([0-9]+) (.*)$');
      if (!itemsNeededMatch) {
        throw new Error(`failed to match itemsNeeded: ${row}`);
      }
      let name = itemsNeededMatch[2];
      const quantity = Number(itemsNeededMatch[1]);
      if (quantity > 1) {
        // Bones Powders => Bones Powder
        name = name.substr(0, name.length -1);
      }
      if (!item.items) {item.items = {};}
      item.items[name] = quantity;
      return;
    }
    case Sections.tools: {
      if (!item.tools) {item.tools = [];}
      item.tools.push(row);
      return;
    }
    case Sections.nexuses: {
      const nexusMatch = row.match('^([A-Za-z]+): ([0-9]+)$');
      if (!nexusMatch) {
        throw new Error(`failed to match nexuses: ${row}`);
      }
      if (!item.nexuses) {item.nexuses = {};}
      item.nexuses[nexusMatch[1] as Nexus] = Number(nexusMatch[2]);
      return;
    }
    case Sections.xp: {
      const xpMatch = row.match('^([A-Za-z]+) exp: ([0-9]+)$');
      if (!xpMatch) {
        throw new Error(`failed to match xp: ${row}`);
      }
      if (!item.xp) {item.xp = {};}
      item.xp[xpMatch[1] as Skill] = Number(xpMatch[2]);
      return;
    }
    case Sections.skills: {
      const skillsMatch = row.match('^([A-Za-z]+): ([0-9]+)$');
      if (!skillsMatch) {
        throw new Error(`failed to match skills: ${row}`);
      }
      if (!item.skills) {item.skills = {};}
      item.skills[skillsMatch[1] as Skill] = Number(skillsMatch[2]);
      return;
    }
    case Sections.knowledge:
      if (!item.knowledge) {item.knowledge = [];}
      item.knowledge.push(row);
      return;
    }
    throw new Error(`UNMATCHED ROW: ${row} - ${openSection}`);
  });

  return item;
};

export default (html: string): Item[] => {
  const items: Item[] = [];
  html.split('<br><br><br>').forEach((itemAsString: string) => itemAsString && items.push(parseItem(itemAsString)));
  return items;
};

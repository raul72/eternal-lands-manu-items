// eslint-disable-next-line import/named
import { StoreOptions } from 'vuex';
import { Item, Skill } from '~/src/types';

interface ItemsState {
  items: Item[],
}

export default {
  state: (): ItemsState => ({
    items: require('../items.json'),
  }),
  getters: {
    getItems: (state: ItemsState): Item[] => state.items,
    getItemsBySkill: (state: ItemsState) =>
      (skill: Skill): Item[] => state.items.filter(item => item.xp?.[skill]),
    getItemByName: (state: ItemsState) =>
      (name: string): Item | undefined => state.items.find(item => item.name === name),
    getItemBySlug: (state: ItemsState) =>
      (hash: string): Item | undefined => state.items.find(item => item.slug === hash),
  },
} as StoreOptions<ItemsState>;
